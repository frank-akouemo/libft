/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fae <fae@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/04 00:29:25 by fakouemo          #+#    #+#             */
/*   Updated: 2022/01/23 14:27:11 by fae              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	*ft_memset(void *str, int c, unsigned int len)
{
	char	*str_char;

	str_char = str;
	while (len-- > 0)
	{
		*str_char = (char)c;
		str_char++;
	}
	return (str);
}
