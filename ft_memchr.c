/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fae <fae@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/16 23:12:38 by fae               #+#    #+#             */
/*   Updated: 2021/11/16 23:40:15 by fae              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//link : https://koor.fr/C/cstring/memchr.wp 

void    *ft_memchr( const void *str, int searchedChar, unsigned int size )
{
	char	*str_find;

	str_find = (char *)str;
	while (size-- > 0 && *str_find != (char)searchedChar)
		str_find++;
	if (*str_find == (char)searchedChar)
		return (str_find);
	return (0);
}
