#include "./../ft_isalpha.c"
#include "./../ft_isdigit.c"
#include "./../ft_isalnum.c"
#include "./../ft_isascii.c"
#include "./../ft_isprint.c"
#include "./../ft_strlen.c"
#include "./../ft_toupper.c"
#include "./../ft_tolower.c"
//#include "./../ft_strncmp.c"
#include "./../ft_strlcpy.c"
#include "./../ft_strnstr.c"
#include "./../ft_memchr.c"
//#include "./../ft_memset.c"
#include "./../ft_memcpy.c"
#include "./../ft_atoi.c"
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>


void test_ft_strlcpy(int size)
{
    char string[] = "Hello there, Venus";
    char buffer[19];
    int r;

    r = strlcpy(buffer,string,size);

    printf("Copied '%s' into '%s', length %d\n",
            string,
            buffer,
            r
          );
}

int main(int argc, char *argv[])
{
    argc = 0;
    argv = 0;
    
    //isalpha test
    printf("is ft_isalpha test\n");
    printf("isalpha('B') : %d vs ft_isalpha('B') : %d\n", isalpha('B'), ft_isalpha('B'));
    printf("isalpha(2)   : %d vs ft_isalpha(2)   : %d\n", isalpha(2)  , ft_isalpha(2));
    printf("isalpha('x') : %d vs ft_isalpha('x') : %d\n", isalpha('x'), ft_isalpha('x'));
    printf("isalpha('-') : %d vs ft_isalpha('-') : %d\n", isalpha('-'), ft_isalpha('-'));
    printf("isalpha('.') : %d vs ft_isalpha('.') : %d\n", isalpha('.'), ft_isalpha('.'));
    printf("\n");

    //isdigit test
    printf("is ft_isdigit test\n");
    printf("isdigit('B') : %d vs ft_isdigit('B') : %d\n", isdigit('B') , ft_isdigit('B'));
    printf("isdigit('8') : %d vs ft_isdigit('8') : %d\n", isdigit('8') , ft_isdigit('8'));
    printf("isdigit( 7 ) : %d vs ft_isdigit( 7 ) : %d\n", isdigit(7)   , ft_isdigit(7));
    printf("\n");
    

    //isalnum test
    printf("is ft_isalnum test\n");
    printf("isalnum('B') : %d vs ft_isalnum('B') : %d\n", isalnum('B') , ft_isalnum('B'));
    printf("isalnum('8') : %d vs ft_isalnum('8') : %d\n", isalnum('8') , ft_isalnum('8'));
    printf("isalnum( 7 ) : %d vs ft_isalnum( 7 ) : %d\n", isalnum(7)   , ft_isalnum(7));
    printf("\n");
    
	//isascii test
    printf("is ft_isascii test\n");
    printf("isascii('B') : %d vs ft_isascii('B') : %d\n", isascii('B') , ft_isascii('B'));
    printf("isascii('8') : %d vs ft_isascii('8') : %d\n", isascii('8') , ft_isascii('8'));
	// hex 0x80 is € which is not printable
    printf("isascii( € ) : %d vs ft_isascii( € ) : %d\n", isascii(0x80) , ft_isascii(0x80));
    printf("\n");

	//isprint test
    printf("is ft_isprint test\n");
    printf("isprint('B') : %d vs ft_isprint('B') : %d\n", isprint('B') , ft_isprint('B'));
    printf("isprint('8') : %d vs ft_isprint('8') : %d\n", isprint('8') , ft_isprint('8'));
    printf("isprint( 2 ) : %d vs ft_isprint( 2 ) : %d\n", isprint(2)   , ft_isprint(2));
    printf("\n");

	//strlen test
    printf("is ft_strlen test\n");
    printf("strlen(\"\") : %lu vs ft_strlen(\"\") : %d\n", strlen("") , ft_strlen(""));
    printf("strlen(\"8\") : %lu vs ft_strlen(\"8\") : %d\n", strlen("8") , ft_strlen("8"));
    printf("strlen(\"2fe\") : %lu vs ft_strlen(\"2fe\") : %d\n", strlen("2fe")   , ft_strlen("2fe"));
    printf("\n");

	//toupper test
    printf("is ft_toupper test\n");
    printf("toupper('B') : %d vs ft_toupper('B') : %d\n", toupper('B') , ft_toupper('B'));
    printf("toupper('b') : %d vs ft_toupper('b') : %d\n", toupper('b') , ft_toupper('b'));
    printf("toupper('8') : %d vs ft_toupper('8') : %d\n", toupper('8') , ft_toupper('8'));
    printf("toupper('c') : %d vs ft_toupper('c') : %d\n", toupper('c') , ft_toupper('c'));
    printf("\n");

    //tolower test
    printf("is ft_tolower test\n");
    printf("tolower('B') : %d vs ft_tolower('B') : %d\n", tolower('B') , ft_tolower('B'));
    printf("tolower('b') : %d vs ft_tolower('b') : %d\n", tolower('b') , ft_tolower('b'));
    printf("tolower('8') : %d vs ft_tolower('8') : %d\n", tolower('8') , ft_tolower('8'));
    printf("tolower('c') : %d vs ft_tolower('c') : %d\n", tolower('c') , ft_tolower('c'));
    printf("\n");


    //ft_strncp
    printf("is ft_strncp test\n");
    char str1[15];
    char str2[15];
    char str3[15];
    char str4[15];
    int ret;
    int ret2;

    strcpy(str1, "abcdef");
    strcpy(str2, "abcdefacBCDEF");
    strcpy(str3, "abcdef");
    strcpy(str4, "abcdefacBCDEF");
    
    ret2 = ft_strncmp(str1, str2, 4);
    ret = strncmp(str1, str2, 4);
    
    printf("strncmp(\"B\") : %d vs ft_strncmp(\"B\") : %d\n", ret , ret2);

    printf("\n");

    //strlcpy test
    printf("is ft_strlcpy test\n");
    test_ft_strlcpy(10);

    printf("\n");

    //strnstr test
    printf("is ft_strnstr test\n");
    const char haystack[20] = "TutorialsPoint";
   const char needle[10] = "Point";
   char *result;

   result = ft_strnstr(haystack, needle, 2);

   printf("The substring is: %s\n", result);

    printf("\n");

    //ft_memchr test
    printf("is ft_memchr test\n");

    char data[] = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
    const unsigned int size = 10;

    // On recherche une valeur inhéxistante :
    void * found = ft_memchr( data, 57, size );
    printf( "57 is %s\n", ( found != NULL ? "found" : "not found" ) );

    // On recherche une valeur existante :
    found = ft_memchr( data, 50, size );
    printf( "50 is %s\n", ( found != NULL ? "found" : "not found" ) );
    if ( found != NULL ) {
        printf( "La valeur à la position calculée est %d\n", *((char *) found) );
    }
    
    printf("\n");

    //ft_memcpy test
    printf("is ft_memchr test\n");

    const char src[50] = "http://www.tutorialspoint.com";
    char dest[50];
    strcpy(dest,"Heloooo!!");
    printf("Before memcpy dest = %s\n", dest);
    ft_memcpy(dest, src, strlen(src)+1);
    printf("After memcpy dest = %s\n", dest);

    printf("\n");  

	//memset test
    printf("is ft_memset test\n");
    //printf("memset(\"B\") : %d vs ft_memset(\"B\") : %d\n", memset("B") , ft_memset("B"));
    //printf("memset(\"8\") : %d vs ft_memset(\"8\") : %d\n", memset("8") , ft_memset("8"));
    //printf("memset( 2 ) : %d vs ft_memset( 2 ) : %d\n", memset(2)   , ft_memset(2));
    printf("\n");
	
	//end of main
	return 0;


}
