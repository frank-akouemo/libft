/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fae <fae@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/15 23:22:44 by fae               #+#    #+#             */
/*   Updated: 2021/11/16 00:19:06 by fae              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int  ft_strlcpy(char *dst, const char *src, unsigned int dstsize)
{
    unsigned int len;

    len = 0;
    while(*src && len + 1 < dstsize)
    {
        *dst = *src;
        src++;
        dst++;
        len++;
    }
    dst[len + 1] = 0;
    while (*src)
    {
        len++;
        src++;
    }
    return (len);
}
