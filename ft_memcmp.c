/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fae <fae@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/16 23:41:59 by fae               #+#    #+#             */
/*   Updated: 2021/11/16 23:49:02 by fae              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//https://www.tutorialspoint.com/c_standard_library/c_function_memcmp.htm

int	ft_memcmp(const void *str1, const void *str2, unsigned int n)
{

	if (n <= 0)
        return (0);
    while(n > 0 && *(char *)str1 && *(char *)str2 )
    {
        if (!((*(char *)str1 - *(char *)str2) == 0))
            return (*(char *)str1 - *(char *)str2);
        str1++;
        str2++;
        n--;
    }
    return (*(char *)str1 - *(char *)str2);
}
