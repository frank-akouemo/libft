/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fae <fae@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/14 18:55:45 by fae               #+#    #+#             */
/*   Updated: 2021/11/16 23:41:14 by fae              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//https://www.tutorialspoint.com/c_standard_library/c_function_strncmp.htm

int ft_strncmp(const char *s1, const char *s2, unsigned int n)
{
    if (n <= 0)
        return (0);
    while(n > 0 && *s1 && *s2 )
    {
        if (!((*s1 - *s2) == 0))
            return (*s1 - *s2);
        s1++;
        s2++;
        n--;
    }
    return (*s1 - *s2);
}