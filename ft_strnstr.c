/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fae <fae@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/16 00:31:42 by fae               #+#    #+#             */
/*   Updated: 2021/11/16 00:48:31 by fae              ###   ########.fr       */
/*   link / https://www.tutorialspoint.com/c_standard_library/c_function_strstr.htm                                                                       */
/* ************************************************************************** */

//https://www.tutorialspoint.com/c_standard_library/c_function_strstr.htm
//https://stackoverflow.com/questions/23999797/implementing-strnstr

int ft_strncmp(const char *s1, const char *s2, unsigned int n)
{
    if (n <= 0)
        return (0);
    while(n > 0 && *s1 && *s2 )
    {
        if (!((*s1 - *s2) == 0))
            return (*s1 - *s2);
        s1++;
        s2++;
        n--;
    }
    return (*s1 - *s2);
}

unsigned int  ft_strnlen(const char *s, unsigned int n){
    unsigned int i;

    i = 0;
    while (*s && i <= n)
    {
        i++;
        s++;
    }
    return i;
}

char *ft_strnstr(const char *haystack, const char *needle, unsigned int len)
{
        int i;
        unsigned int needle_len;

        if (0 == (needle_len = ft_strnlen(needle, len)))
                return (char *)haystack;

        i = 0;
        while (i<=(int)(len-needle_len))
        {
                if ((haystack[0] == needle[0]) &&
                        (0 == ft_strncmp(haystack, needle, needle_len)))
                        return (char *)haystack;

                haystack++;
                i++;
        }
        return (0);
}