/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fae <fae@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/16 23:49:36 by fae               #+#    #+#             */
/*   Updated: 2021/11/17 00:09:22 by fae              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	*ft_memcpy(void *dest, const void * src, unsigned int n)
{
	void *res;
	unsigned int len;

    len = 0;
	res = dest;
    while(*(char *)src && len + 1 < n)
    {
        *(char *)dest = *(char *)src;
        src++;
        dest++;
        len++;
    }
	dest++;
    dest = 0;
    return (res);
}
